import {
    // Button,
    // Col,
    // Row,
    Popup,
    // Checkbox,
    // CheckboxGroup,
    // Field,
    // Picker,
    // RadioGroup,
    // Radio,
    // Search,
    // ActionSheet,
    Dialog,
    Notify,
    // PullRefresh,
    Toast,
    // Divider,
    // Empty,
    // List,
    // Progress,
    NavBar,
    // Pagination,
    // Tab,
    // Tabs,
    // Sticky,
    // DatetimePicker,
    // Panel,
    Cell,
    CellGroup,
    Icon,
    Switch,
    // Skeleton,
} from "vant";

//引入 vant 组件
//import {Dialog, Button, Switch, Cell, Col, Row, Search, Swipe, SwipeItem} from "vant";
//引入 vant 函数组件
//import { showToast, showFailToast, showNotify } from 'vant';

// import '@/doc/style.css'
//import "vant/lib/index.css";
// app.use(Dialog);
// app.use(Button);

const components = [
    // Button,
    // Col,
    // Row,
    Popup,
    // Checkbox,
    // CheckboxGroup,
    // Field,
    // Picker,
    // RadioGroup,
    // Radio,
    // Search,
    // ActionSheet,
    Dialog,
    Notify,
    // PullRefresh,
    Toast,
    // Divider,
    // Empty,
    // List,
    // Progress,
    NavBar,
    // Pagination,
    // Tab,
    // Tabs,
    // Sticky,
    // DatetimePicker,
    // Panel,
    Cell,
    CellGroup,
    Icon,
    Switch,
    // Skeleton,
];

const install = (app) => {
    components.forEach((item) => {
        app.component(item.name, item);
    });
};

if (typeof window !== "undefined" && window.Vue) {
    install(window.Vue);
}
export default { install };
