import {
    // ElAlert,
// ElAutocomplete,
// ElAvatar,
// ElBacktop,
// ElBadge,
// ElBreadcrumb,
// ElBreadcrumbItem,
    ElButton,
    ElIcon,
// ElButtonGroup,
// ElCalendar,
// ElCard,
 ElCarousel,
 ElCarouselItem,
// ElCascader,
// ElCascaderPanel,
// ElCheckbox,
// ElCheckboxButton,
// ElCheckboxGroup,
// ElCollapse,
// ElCollapseItem,
// ElCollapseTransition,
// ElColorPicker,
// ElDatePicker,
// ElDialog,
// ElDivider,
// ElDrawer,
    ElDropdown,
    ElDropdownItem,
    ElDropdownMenu,
// ElFooter,
// ElIcon,
// ElImage,
    ElInput,
// ElInputNumber,
// ElLink,
// ElMenu,
// ElMenuItem,
// ElMenuItemGroup,
// ElOption,
// ElOptionGroup,
// ElPageHeader,
// ElPagination,
// ElProgress,
// ElRadio,
// ElRadioButton,
// ElRadioGroup,
// ElRate,
// ElRow,
// ElScrollBar,
// ElSelect,
// ElSlider,
// ElStep,
// ElSteps,
//    ElSubmenu,
    ElSwitch,
// ElTimeline,
// ElTimelineItem,
    ElTooltip,
// ElTransfer,
// ElTree,
// ElUpload,
// ElInfiniteScroll,
// ElLoading,
// ElMessage,
    ElMessageBox,
    ElNotification,
    ElMessage,
    ElLoading,

    ElContainer,
    ElAside,
    ElScrollbar,
    ElRow,
    ElCol,

    ElTable,
    ElTableColumn,
} from 'element-plus'

export default (app) => {
    app.use(ElButton)
    app.use(ElIcon)
    app.use(ElNotification)
    app.use(ElDropdown)
    app.use(ElDropdownItem)
    app.use(ElDropdownMenu)

    app.use(ElSwitch)
    app.use(ElTooltip)
    app.use(ElMessageBox)
    app.use(ElLoading)
    app.use(ElMessage)

    app.use(ElInput)
    app.use(ElContainer)
    app.use(ElAside)
    app.use(ElScrollbar)
    app.use(ElRow)
    app.use(ElCol)

    app.use(ElCarousel)
    app.use(ElCarouselItem)

    app.use(ElTable)
    app.use(ElTableColumn)
    //app.config.globalProperties.$message = ElMessage
}

