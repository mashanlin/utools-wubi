/**
 * 查找所有汉字（,分隔）
 * @param str
 * @returns {*}
 */
function searchZHList(word) {
    var pattern1 = /[\u4e00-\u9fa5]+/g;
    var list = word.match(pattern1);
    var arr = new Array();
    if(list == null || list.length == 0) {
        return arr;
    }
    var allStr = "";
    for (var i = 0; i < list.length; i++) {
        var item = list[i];
        for (var j = 0; j < item.length; j++) {
            var z = item[j];
            if(allStr.indexOf(z) >= 0) {
                continue;
            }
            allStr += z;
            arr.push(z);
        }
    }
    return arr;
}

/**
 * 查找所有中文
 * @param word
 * @returns {Promise<unknown>}
 */
export const findZHList = (word) => {
    console.log("🐛:: findZHList -> word", word);
    return new Promise((resolve, reject) => {
        let res = searchZHList(word);
        if (res) {
            console.log("🐛:: findZHList -> res", res);
            resolve(res);
        } else {
            reject("出错");
        }
    });
};