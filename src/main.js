import { createApp } from 'vue'
import App from '@/App.vue'

//import utools from "@/utils/utools.js";
import Global from "@/utils/global.js";
import Api from "@/utils/index";

const app = createApp(App);

//------------element-plus start----------------
import 'element-plus/dist/index.css'
import installElementPlus from "@/utils/element.js";
import * as ElIcons from '@element-plus/icons-vue'

installElementPlus(app)
for (let iconName in ElIcons) {
    app.component(iconName, ElIcons[iconName])
}
//------------element-plus end----------------
//
// app.config.globalProperties.$toast = showToast;

//app.config.productionTip = false;

window.utools = window.utools || Global;

//全局引入
app.config.globalProperties.$api = Api;
app.config.globalProperties.$global = Global;

app.mount("#app");
