const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 根路径 默认使用/ vue cli 3.3+ 弃用 baseUrl
  publicPath: './', // 此处改为 './' 即可
  productionSourceMap: false,
  outputDir: './data/dist',
})
