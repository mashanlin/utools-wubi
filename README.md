
### 介绍
<img src="src/assets/logo.png" width="15"/> utools五笔编码插件

### 软件架构
软件架构说明


### 安装教程

该插件是 utools 插件，可以打包成本地插件安装使用，也可以在 utools 插件库中搜索安装

### 展示图如下
![image](src/assets/show-img2.png)

![image](src/assets/show-img1.png)

### 打赏
开发不易,您的鼓励会让我走的更远,一分也是情一分也是爱❤️

![打赏](src/assets/pay_wx.png)

## 关注我

觉得不错的话给个star
